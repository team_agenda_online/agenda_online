<?php
	require 'inc/functions.php';
	session_start();
		
	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se'l redirigeix a 'no_session.php'
	if ( !isset($_SESSION['username']) )
		header('location:no_session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<title>Cercador - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/page.css">
	<link rel="stylesheet" type="text/css" href="css/edit.css">
	<link href="css/contactsTable.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-1.9.1.min.js"></script>
</head>
<body>
	<div id="container">
		<div id="header">
			<span id="topButtons" style="float: right;"><a href="logout.php"><img src="img/logout_20_white.png">Desconnectar-se</a></span>
			<span id="topButtons" style="float: left;"><a href="page.php"><img src="img/home_20_white.png">Inici</a></span>
		</div>
		<div id="content">
				
			<span id="add">
				<a href="inscribe.php"><img src="./img/add_20.png"> Afegeix un contacte</a>
			</span> 
			
			<form name="searchForm" accept-charset="utf-8" method="POST" style="margin: 5%;">
				<input type="checkbox" name="myBoxes" value="surname" onclick="buscar()" > Cognoms <section>
				<input type="checkbox" name="myBoxes" value="email" onclick="buscar()" > Correu <section>
				<input type="checkbox" name="myBoxes" value="phone"  onclick="buscar()"> Telèfon <section>
				<input type="checkbox" name="myBoxes" value="address" onclick="buscar()"> Adreça <section>
				<input type="checkbox" name="myBoxes" value="birthday" onclick="buscar()"> Data naixement <section>
				
				<br>
				<input 
					type="text" 
					name="busqueda" 
					id="busqueda" 
					value="" 
					placeholder="cerca..." 
					maxlength="30" 
					autocomplete="off"
					onkeyup="buscar();" 
				> 
			</form>
		
			<div id="id_divResultatsBusqueda" style="text-align:center;"> </div>
			
<?php
		// Obtenim el div modal per tenir-lo preparat en cas que es vulgui editar
		echo ( getModalDivToEditContact() );
		
		// Encara que tenim al el div modal al document, no el mostrem fins que es fa click a l'icona Edit
		if ($_POST)
			echo '<script>document.getElementsByClassName("modal")[0].style.display = "block";</script>';
?>
</div>
</div>

<script>
$(document).ready(function() {
    $("#id_divResultatsBusqueda").html('<p>No s\'està realitzant cap búsqueda</p>');
});


function buscar() {
	 // La variable obtindrà el valor del camps [INPUT] amb el ID [busqueda]
    var textBusqueda = $("input#busqueda").val();  
 
	 var auxBoxes = "";
    var checkBoxes = document.forms['searchForm']['myBoxes'];
    for (i=0; i<checkBoxes.length; i++){
    	if (checkBoxes[i].checked)
    		auxBoxes += checkBoxes[i].value + ", ";
    }
 
	 // Si el camp està buit no s'ejecutarà el jQuery
	 // Sinó, per cada tecla (onkeyup="buscar();") s'executarà la consulta a la BD per mostrar-la
	 // A l'arxiu PHP s'enviarà dues dades en forma de $_POST
	 //	→ valorBusqueda: textBusqueda (valor del camps de text)
	 //	→ valorBoxes: auxBoxes (string amb els paràmetres dels checkBoxes seleccionats en aquell moment)
    if (textBusqueda != "") {
       $.post("search_ajax.php", {valorBusqueda: textBusqueda, valorBoxes: auxBoxes}, function(mensaje) {
           $("#id_divResultatsBusqueda").html(mensaje);
        }); 
    } else { 
       $("#id_divResultatsBusqueda").html('<p>No s\'està realitzant cap búsqueda</p>');
    };
};
</script>
<script type="text/javascript" src="js/edit.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
</body>
</html>