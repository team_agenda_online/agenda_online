<?php

require 'inc/functions.php';
session_start();

// Analitzem l'arxiu de configuració per connectar-nos a la base de dades
$strIniFile = "inc/dbConf.ini";
$array_ini = parse_ini_file($strIniFile, true);

$serverNameOrIpAddr = $array_ini['serverNameOrIpAddr'];
$userRoot = $array_ini['userRoot'];
$passwordRoot = $array_ini['passwordRoot'];
$dbName = $array_ini['dbName'];		
$dbUserTableName = $array_ini['dbUserTableName'];
$dbContactsTableName = $array_ini['dbContactsTableName'];
$conn = mysqli_connect($serverNameOrIpAddr, $userRoot, $passwordRoot, $dbName);

// Variable de búsqueda
$consultaBusqueda = $_POST['valorBusqueda'];

// Filtro anti-XSS
$caracteres_malos = array("<", ">", "\"", "'", "/", "<", ">", "'", "/");
$caracteres_buenos = array("& lt;", "& gt;", "& quot;", "& #x27;", "& #x2F;", "& #060;", "& #062;", "& #039;", "& #047;");
$consultaBusqueda = str_replace($caracteres_malos, $caracteres_buenos, $consultaBusqueda);

if (isset($consultaBusqueda)) {

	$limit = 5;
	if ( isset($_SESSION['page']) )
		$offset = ($_SESSION['page'] - 1) * $limit;
	else
		$offset = 1 * $limit ;
	global $dbUserTableName, $dbContactsTableName, $conn;
	
	// Capçalera de la taula de resultats	
	$contactsTable = "";
	$contactsTable .= '<div class="rTable">';
	$contactsTable .= '	<div class="rTableRow">';
	$contactsTable .= '		<div class="rTableHead"><strong>Nom</strong></div>';
	$contactsTable .= '		<div class="rTableHead"><strong>Cognoms</strong></div>';
	$contactsTable .= '		<div class="rTableHead"><strong>Correu</strong></div>';
	$contactsTable .= '		<div class="rTableHead"><strong>Telèfon</strong></div>';
	$contactsTable .= '		<div class="rTableHead"><strong>Adreça</strong></div>';
	$contactsTable .= '		<div class="rTableHead"><strong>Data naixement</strong></div>';
	$contactsTable .= '	</div>';
	
	$userId = $_SESSION['userId'];
	
	// Comprovem les opcions de búsqueda a través dels checkboxes 
	$opcionsBusqueda = $_POST['valorBoxes'];
	
	$surnameCheck = $emailCheck = $phoneCheck = $addressCheck = $birthdayCheck = false; 
	if ( substr_count($opcionsBusqueda, "surname")>0 )
		$surnameCheck = true;
	if ( substr_count($opcionsBusqueda, "email")>0 )
		$emailCheck = true;
	if ( substr_count($opcionsBusqueda, "phone")>0 )
		$phoneCheck  = true;
	if ( substr_count($opcionsBusqueda, "address")>0 )
		$addressCheck = true;
	if ( substr_count($opcionsBusqueda, "birthday")>0 )
		$birthdayCheck = true;
		
	// I realitzem la consulta SQL que s'indiquin als checkBoxes
	$sql = "SELECT * FROM ".$dbContactsTableName." WHERE name LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
	if ($surnameCheck) {	
		$sql .= " OR surname LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
		$sql .= " OR CONCAT(name,' ',surname) LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
	}
	if ($emailCheck)	
		$sql .= " OR email LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
	if ($phoneCheck)	
		$sql .= " OR phone LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
	if ($addressCheck)	
		$sql .= " OR address LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'";
	if ($birthdayCheck)	
		$sql .= " OR birthday LIKE '%$consultaBusqueda%' AND userId = '".$_SESSION['userId']."'"; 
	
	$result = mysqli_query($conn, $sql);
	
	// Si no troba cap resultat per a la consulta anterior, mostrem missatge d'error
	if (mysqli_num_rows($result) === 0) {
		$contactsTable = "<p>No existeix cap usuari amb paràmetres semblants a <strong>".$consultaBusqueda."</strong></p>";
	} else {
		// S'informa de la cerca i dels paràmetres actius sobre la mateixa
		echo 'Resultats per <br>[ <span style="background-color:cyan;">'.$consultaBusqueda.'</span> ]<br>';
		echo 'Paràmetres de la búsqueda [<strong>nom</strong> per defecte]: <strong>'.$opcionsBusqueda.'</strong>';
		
		// Si per exemple s'ha activat l'opció Cognoms, a cada cerca es remarcarà les coincidències d'aquella columna 
		while($row = mysqli_fetch_assoc($result)){
			$name = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['name']) );
			if ($surnameCheck) $surname = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['surname']) );
			else $surname = $row['surname'];
			
			if ($emailCheck) $email = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['email']) );
			else $email = $row['email'];
				
			if ($phoneCheck) $phone = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['phone']) );
			else $phone = $row['phone'];
					
			if ($addressCheck) $address = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['address']) );
			else $address = $row['address'];
			
			if ($birthdayCheck) $birthday = remarkSearchedValue( strtolower($consultaBusqueda), strtolower($row['birthday']) );
			else $birthday = $row['birthday'];
			
			// Mostrem els camps i juntem el formulari per editar i eliminar el contacte de la tupla
			$contactsTable .= '<div class="rTableRow">';
			$contactsTable .= '	<div class="rTableCell">'.$name.'</div>';
			$contactsTable .= '	<div class="rTableCell">'.$surname.'</div>';
			$contactsTable .= '	<div class="rTableCell">'.$email.'</div>';
			$contactsTable .= '	<div class="rTableCell">'.$phone.'</div>';
			$contactsTable .= '	<div class="rTableCell">'.$address.'</div>';
			$contactsTable .= '	<div class="rTableCell">'.$birthday.'</div>';
			$contactsTable .= '	<div class="rTableCell">
				<form method="POST" action="search.php">
					<input type="hidden" name="contactId" value="'.$row['contactId'].'"/>
					<input type="hidden" name="userId" value="'.$row['userId'].'"/>
					<input type="image" value="submit" name="edit" src="img/edit_20.png" >
				</form></div>';
			$contactsTable .= '	<div class="rTableCell">
				<form method="POST" action="delete.php">
					<input type="hidden" name="contactId" value="'.$row['contactId'].'"/>
					<input type="image" name="delete" value="submit" src="img/delete_20.png" onclick="return confirm(\'Realment vols eliminar aquest contacte?\')" >
				</form></div>';
			$contactsTable .= '</div>';
		}; // Fi while $result

	}; // Fi else num_rows > 0

}; // Fi isset $consultaBusqueda

// Retornem la taula
echo $contactsTable;

?>