function showIframe() {
	var calendarIframe = document.getElementsByClassName("divIframe")[0];
	if (calendarIframe.style.display == "block")
		calendarIframe.style.display = "none";
	else
		calendarIframe.style.display = "block"
}


function iniciCal() {
	date = new Date();
	mes = date.getMonth();
 	any = date.getFullYear();
 	calendar(0,0,mes,any);
}


function calendar(mesMoure, anyMoure, mes, any) {

	var calendarDIV = document.getElementsByClassName("calendarDIV")[0];
	var mesosDIV = document.getElementsByClassName("mesosDIV")[0];
	
	checkPrimerDia = 1
	diaContador = 0;
	date = new Date();
	
	dia = date.getDate();
	mes = mes + mesMoure;
 	any = any + anyMoure;
	
	if (mes < 0) {
		mes = 11;
		any = any - 1;
	}
	else if (mes > 11) {
		mes = 0;
		any = any + 1;		
	}	
	
	mesActual = new Date(any, mes);
	//console.log(mesActual);
	mesSeguent = new Date(any, mes + 1);
	
	primerDia = mesActual.getDay();
	diesDelMes = Math.round((mesSeguent.getTime() - mesActual.getTime()) / (1000 * 60 * 60 * 24));
	
	var divMesos = document.createElement("div");
	mesosDIV.appendChild(divMesos);
	divMesos.setAttribute("id","contenidorMesos");
	
	var mesEnrere = document.createElement("img");
	divMesos.appendChild(mesEnrere);
	mesEnrere.setAttribute("src","../img/back_20.png");
	mesEnrere.setAttribute("onclick", "mesEnrere(-1,0,"+mes+","+any+")");
	
	mesos = new Array('Gener','Febrer','Març','Abril','Maig','Juny','Juliol','Agost','Setembre','Octubre','Novembre','Desembre');
	
	var mesActual = document.createTextNode(mesos[mes]);
	divMesos.appendChild(mesActual);
	
	var mesEnvant = document.createElement("img");
	divMesos.appendChild(mesEnvant);
	mesEnvant.setAttribute("src","../img/front_20.png");
	mesEnvant.setAttribute("onclick", "mesEnrere(1,0,"+mes+","+any+")");
	
	var anyEnrere = document.createElement("img");
	divMesos.appendChild(anyEnrere);
	anyEnrere.setAttribute("src","../img/back_20.png");
	anyEnrere.setAttribute("onclick", "mesEnrere(0,-1,"+mes+","+any+")");
	anyEnrere.setAttribute("style","margin-left: 20%;");
	
	var anyActual = document.createTextNode(any);
	divMesos.appendChild(anyActual);
	
	var anyEnvant = document.createElement("img");
	divMesos.appendChild(anyEnvant);
	anyEnvant.setAttribute("src","../img/front_20.png");
	anyEnvant.setAttribute("onclick", "mesEnrere(0,1,"+mes+","+any+")");
	
	var divContenidor = document.createElement("div");
	calendarDIV.appendChild(divContenidor);
	divContenidor.setAttribute("id","contenidor");

	for (i=0; i<5; i++) {
		var divFila = document.createElement("div");
		divContenidor.appendChild(divFila);
		divFila.setAttribute("id","fila");
		
		for (j=0; j<7; j++) {
			if (checkPrimerDia != primerDia) {
				if (primerDia == 0) {
					primerDia = 7;
				}
				var divColumna = document.createElement("div");
				divFila.appendChild(divColumna);
				divColumna.setAttribute("id","columna");
				checkPrimerDia++;
				diaBuit = '';
				var diaBuit = document.createTextNode(diaBuit.toString());
				divColumna.appendChild(diaBuit);
			} else {
				if (diaContador < diesDelMes) {
					diaContador++;
				} else {
					break; 
				}
				var divColumna = document.createElement("div");
				divFila.appendChild(divColumna);
				divColumna.setAttribute("id","columna");
				divColumna.setAttribute("onclick","mostraDia("+diaContador+","+mes+","+any+")");
				var diaMes = document.createTextNode(diaContador.toString());
				divColumna.appendChild(diaMes);
			}
		}
	}
}


function mesEnrere(mesMoure, anyMoure, mes, any) {
	var aux = document.getElementById("contenidor");
	aux.parentNode.removeChild(aux);
	var aux2 = document.getElementById("contenidorMesos");
	aux2.parentNode.removeChild(aux2);
	document.getElementsByClassName("calendarDIV").innerHTML = calendar(mesMoure,anyMoure,mes,any);
}


function mostraDia(dia,mes,any) {
	mes = mes +1;
	parent.document.getElementsByName("birthday")[0].value=any+"-"+mes+"-"+dia;
	parent.document.getElementsByClassName("divIframe")[0].style.display = "none";
}