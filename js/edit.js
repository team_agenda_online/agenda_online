// Agafaem el div modal
var modal = document.getElementById('myModal');

// Busquem l'span 'X' que tanca
var span = document.getElementsByClassName("close")[0];
span.onclick = function() {
    modal.style.display = "none";
}

// Si l'usuari fa clic a qualsevol lloc que no sigui la finestra modal d'edició de contacte, es tancarà
window.onclick = function(event) {
	if (event.target == modal)
		modal.style.display = "none";
}