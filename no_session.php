<?php
	require 'inc/functions.php';
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Inici - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/page.css">
	<link rel="stylesheet" type="text/css" href="css/edit.css">
	<link href="css/contactsTable.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">
	<div id="header">
		<span id="topButtons" style="float: left;"><a href="index.php"><img src="img/home_20_white.png">Inici</a></span>
	</div>
	
	<div id="content">
		<p style='padding: 5%;'>
			No has iniciat sessió <br>
			Se't redirigeix a la pàgina d'inici en 5 segons, si no funciona fes <a href='index.php'>clic aquí</a>
		</p>
		
		<?php
			header( "refresh:5;url=index.php" );
		?>
	</div>
</div>
</body>	





