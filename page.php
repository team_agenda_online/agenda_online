<?php
	require 'inc/functions.php';
	session_start();
	
	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se'l redirigeix a 'no_session.php'
	if ( !isset($_SESSION['username']) )
		header('location:no_session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Inici - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/page.css">
	<link rel="stylesheet" type="text/css" href="css/edit.css">
	<link href="css/contactsTable.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">
	<div id="header">
		<span id="topButtons" style="float: right;"><a href="logout.php"><img src="img/logout_20_white.png">Desconnectar-se</a></span>
		<span id="topButtons" style="float: left;"><a href="page.php"><img src="img/home_20_white.png">Inici</a></span>
	</div>

	<div id="content">
		<div id="cercador">
			<a href="search.php"><img src="img/search_20.png">Cercador</a>
		</div>
	
	<span id="add"><a href="inscribe.php"><img src="./img/add_20.png">Afegeix un contacte</a></span>
	<h1>Hola <?php echo $_SESSION['username']?> !</h1>
	<h3>Els teus contactes</h3>
	
<?php
		// Mostrem els spans per recórrer el llistat obtingut
		if ( isset($_GET['page']) )
			echo ( getNextPrevList($_GET['page']) );
		else
			echo ( getNextPrevList(0) );
		
		// Mostrem la taula en forma de text que ens retorna la funció
		echo ( getContactsTable() );
		
		// Obtenim el div modal per tenir-lo preparat en cas que es vulgui editar
		echo ( getModalDivToEditContact() );
		
		// Encara que tenim al el div modal al document, no el mostrem fins que es fa click a l'icona Edit
		if ( ($_POST) )
			echo '<script>document.getElementsByClassName("modal")[0].style.display = "block";</script>';
?>
	</div>
</div>

<script type="text/javascript" src="js/edit.js"></script>
<script type="text/javascript" src="js/calendar.js"></script>
</body>
</html>