<?php 
	require 'inc/functions.php';
	session_start();
	
	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se'l redirigeix a 'no_session.php'
	if ( !isset($_SESSION['username']) )
		header('location:no_session.php');
?>
<html>
<head>
	<meta charset="utf-8" />
	<title>Actualitza dades - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/inscribe.css">
</head>
<body>
<div id="container">
	<div id="header">
		<span id="topButtons" style="float: right;"><a href="logout.php"><img src="img/logout_20_white.png">Desconnectar-se</a></span>
		<span id="topButtons" style="float: left;"><a href="page.php"><img src="img/home_20_white.png">Inici</a></span>
	</div>
	<div id="content">
<?php
	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se l'avisa i es redirigeix a 'index.php'
	if ( !isset($_SESSION['username']) ){
		header('location:no_session.php');
	} else if ($_POST){
		updateContact();
		header("refresh:5;url=page.php");
	}
?>	
	</div>
</div>
<script type="text/javascript" src="js/calendar.js"></script>
</body>
</html>