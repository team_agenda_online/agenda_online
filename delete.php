<?php
	require 'inc/functions.php';
	session_start();

	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se'l redirigeix a 'no_session.php'
	if ( !isset($_SESSION['username']) ){
		header('location:no_session.php');
	} else if ($_POST) {
		deleteContactUser( $_POST['contactId'] );
		header("location:page.php");
	}
?>

</body>
</html> 