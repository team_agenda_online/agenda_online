<?php

// Analitzem l'arxiu de configuració per connectar-nos a la base de dades
$strIniFile = "inc/dbConf.ini";
$array_ini = parse_ini_file($strIniFile, true);

$serverNameOrIpAddr = $array_ini['serverNameOrIpAddr'];
$userRoot = $array_ini['userRoot'];
$passwordRoot = $array_ini['passwordRoot'];
$dbName = $array_ini['dbName'];		
$dbUserTableName = $array_ini['dbUserTableName'];
$dbContactsTableName = $array_ini['dbContactsTableName'];
$conn;
		  
		  
function validateUser() {
	
	global $dbUserTableName, $conn;
	$isUser = false;
	if ( openDB() ){
		
		// Busquem l'userId a partir del userName per mantindre-ho al $_SESSION		
		$sql = "SELECT * FROM ".$dbUserTableName." WHERE userName = '".$_POST['username']."' AND passMd5 = md5('".$_POST['password']."')";
		
		$result = mysqli_query($conn, $sql);
		
		if (mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				$_SESSION['userId'] = $row['userId'];
			}
			$isUser = true;
		}			

		// Tanquem DB		
		closeDB();
	
		// Comprovem si existeix l'Usuari i retornem True sinó, False
		if ($isUser)
			return true;
			
		return false;
	}
}


function getContactsTable() {
		
	$limit = 5;
	if ( isset($_SESSION['page']) )
		$offset = ($_SESSION['page'] - 1) * $limit;
	else
		$offset = 1 * $limit ;
	global $dbUserTableName, $dbContactsTableName, $conn;
	$contactsTable = "";
	$userId = $_SESSION['userId'];
	
	if ( openDB() ){	
		$sql = "SELECT * FROM ".$dbContactsTableName." WHERE userId = '".$userId."' LIMIT ".$limit." OFFSET ".$offset;
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0){
			$contactsTable .= '<div class="rTable">';
			$contactsTable .= '	<div class="rTableRow">';
			$contactsTable .= '		<div class="rTableHead"><strong>Nom</strong></div>';
			$contactsTable .= '		<div class="rTableHead"><strong>Cognoms</strong></div>';
			$contactsTable .= '		<div class="rTableHead"><strong>Correu</strong></div>';
			$contactsTable .= '		<div class="rTableHead"><strong>Telèfon</strong></div>';
			$contactsTable .= '		<div class="rTableHead"><strong>Adreça</strong></div>';
			$contactsTable .= '		<div class="rTableHead"><strong>Data naixement</strong></div>';
			$contactsTable .= '	</div>';
			while($row = mysqli_fetch_assoc($result)){
				// Per cada sortida (tupla) creem un nou div de classe ROW
				$contactsTable .= '<div class="rTableRow">';
				$contactsTable .= '	<div class="rTableCell">'.$row['name'].'</div>';
				$contactsTable .= '	<div class="rTableCell">'.$row['surname'].'</div>';
				$contactsTable .= '	<div class="rTableCell">'.$row['email'].'</div>';
				$contactsTable .= '	<div class="rTableCell">'.$row['phone'].'</div>';
				$contactsTable .= '	<div class="rTableCell">'.$row['address'].'</div>';
				$contactsTable .= '	<div class="rTableCell">'.$row['birthday'].'</div>';
				$contactsTable .= '	<div class="rTableCell">
					<form method="POST" action="page.php">
						<input type="hidden" name="contactId" value="'.$row['contactId'].'"/>
						<input type="hidden" name="userId" value="'.$row['userId'].'"/>
						<input type="image" value="submit" name="edit" src="img/edit_20.png" >
					</form></div>';
				$contactsTable .= '	<div class="rTableCell">
					<form method="POST" action="delete.php">
						<input type="hidden" name="contactId" value="'.$row['contactId'].'"/>
						<input type="image" name="delete" value="submit" src="img/delete_20.png" onclick="return confirm(\'Realment vols eliminar aquest contacte?\')" >
					</form></div>';
				$contactsTable .= '</div>';
			}
			$contactsTable .= '</div>';
		}
		else {
			echo "<div class='noResults'>No s'han trobat contactes. <a href='inscribe.php'>Afegeix-ne! <img src='./img/add_20.png'></a></div>";	
			echo "<script> document.getElementById('add').style.display='none'; </script>";
		}
		
		// Retornem la variable amb tota la taula en forma de text
		return $contactsTable;
	}
}

function closeDB(){
	// Obtenim totes les variables, tanquem la bd i les lliberem 
	global $serverNameOrIpAddr, $userRoot, $passwordRoot, $dbName, $dbUserTableName, $dbContactsTableName, $conn;
	mysqli_close($conn);
	$variables = array($serverNameOrIpAddr, $userRoot, $passwordRoot, $dbName, $dbUserTableName, $dbContactsTableName, $conn);
	$len = count($variables);
	for($i=0; $i<$len; $i++) {
		unset($variables[$i]);	
	}
}


function deleteContactUser($contactId){

	global $dbContactsTableName, $conn;
	
	if (openDB()){
		// Eliminem el contacte de la taula		
		$sql = "DELETE	FROM ".$dbContactsTableName."	WHERE contactId = '".$contactId."' AND userId = '".$_SESSION['userId']."'";
		$result = mysqli_query($conn, $sql);	
	}
	
	closeDB();
}


function openDB() {
	global $serverNameOrIpAddr, $userRoot, $passwordRoot, $dbName, $conn;
	
	// Creem connexió
	$conn = mysqli_connect($serverNameOrIpAddr, $userRoot, $passwordRoot, $dbName);

	// Comprovem connexió
	if (!$conn)
		die("Connection failed: " . mysqli_connect_error());
	
	return true;
}


function alertPhp($msg){
	echo "<script>alert(\"".$msg."\")</script>";
}


function createUser() {
	
	global $dbUserTableName, $dbContactsTableName, $conn;
	$userId = $_SESSION['userId'];
	$name = $_POST['name'];
	$surname = $_POST['surname'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];
	$address = $_POST['address'];
	$birthday = $_POST['birthday'];
	
	if( openDB() ) {
		$sqlInsert = "INSERT INTO ".$dbContactsTableName." (userId, name, surname, email, phone, address, birthday) VALUES ('$userId', '$name', '$surname', '$email', '$phone', '$address', '$birthday')";
		
		if (mysqli_query($conn,$sqlInsert)) {
			echo "<p style='padding: 5%;'>Nou contacte inserit amb èxit!<br>La pàgina et redigirà en 5 segons, si no funciona fes <a href='page.php'>clic aquí</a></p>";
			header( "refresh:5;url=page.php" );
		} else {
			echo "<p style='padding: 5%;'>No es pot inserir! Comprova que tots els camps introduits son correctes i que el contacte no estigui repetit!<br>La pàgina et redigirà en 5 segons, si no funciona fes <a href='page.php'>clic aquí</a></p>";
			header( "refresh:5;url=page.php" );
		}
	}
}


function getContactToEdit() {
	
	global $dbUserTableName, $dbContactsTableName, $conn;
	
	if ( !isset($_POST['contactId']) )
		return false;
		
	$contactId = $_POST['contactId'];
	$userId = $_POST['userId'];
	$contactToEdit = "";
	
	if ( openDB() ){
		
		$sql = "SELECT * FROM ".$dbContactsTableName." WHERE contactId = '".$contactId."' AND userId = '".$userId."'";
		$result = mysqli_query($conn, $sql);
		
		if (mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				$contactToEdit .= '
					<form method="POST" action="actualitza.php">
						<input type="hidden" name="contactId" value="'.$row['contactId'].'"/>
						<input type="hidden" name="userId" value="'.$row['userId'].'"/>
						<input type="text" placeholder="nom" name="name" value="'.$row['name'].'" required> <br><br>
						<input type="text" placeholder="cognoms" name="surname" value="'.$row['surname'].'" required> <br><br>
						<input type="text" placeholder="correu" name="email" value="'.$row['email'].'" required> <br><br>
						<input type="text" placeholder="telèfon" name="phone" value="'.$row['phone'].'" required> <br><br>
						<input type="text" placeholder="adreça" name="address" value="'.$row['address'].'" required> <br><br>
						<input type="text" placeholder="aaaa/mm/dd" name="date" onclick="showIframe()" value="'.$row['birthday'].'" required> <br><br>
						<input type="image" name="update" src="img/save_20.png" value="submit"> Guardar 
					</form>';
			}
		}
		
		$contactToEdit .= '
			<div class="divIframe" style="display: none; margin-left: -100%; margin-bottom: 40%;">
				<iframe name="iframecalendar" scrolling="no" frameborder="0" width="100%" height="100%" src="js/calendar.html" style="position: absolute;" ></iframe>
			</div>';
	}
	
	closeDB();
	
	return $contactToEdit;
}


function updateContact() {
	
	global $dbUserTableName, $dbContactsTableName, $conn;
	
	if ($_POST){
		if ( openDB() ){
			$sql = "
				UPDATE ".$dbContactsTableName." 
				SET name = '".$_POST['name']."' 
				, surname = '".$_POST['surname']."'
				, email = '".$_POST['email']."'
				, phone = '".$_POST['phone']."'
				, address = '".$_POST['address']."'
				, birthday = '".$_POST['date']."' 
				WHERE contactId = '".$_POST['contactId']."'
				AND userId = '".$_POST['userId']."'";
			
			if ( $result = mysqli_query($conn, $sql) ){
				echo "<p style='padding: 5%;'>Contacte actualitzat correctament<br>La pàgina et redigirà en 5 segons, si no funciona fes <a href='page.php'>clic aquí</a></p>";
			} else {
				echo "<p style='padding: 5%;'>Error en actualitzar<br>La pàgina et redigirà en 5 segons, si no funciona fes <a href='page.php'>clic aquí</a></p>";
			}
		}
	}
	
	closeDB();
}


function countContacts() {
	global $dbUserTableName, $dbContactsTableName, $conn;
	$userId = $_SESSION['userId'];
	$num = 0;
	
	if ( openDB() ){
		$sql = "SELECT COUNT(*) AS total FROM ".$dbContactsTableName." WHERE userId = ".$userId;
		$result = mysqli_query($conn, $sql);
		
		if (mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_assoc($result)){
				$num = $row['total'];
			}
		}
	}
	
	return $num;
}


function getNextPrevList($get_) {
		
	$limit = 5;
   $total = countContacts();
   
   $ceil = $total/$limit;
	$pages = ceil($ceil);
	$page = 1;
	echo '<div id="nextPrev">';	
	
	if( empty($get_) && $total>$limit && $total>0 ){
			$page = 1;
			echo "<a id='ant' style='pointer-events: none; color: grey;display:inline-block;' href='?page=$page'><Pàg. Anterior</a>";
			echo "<a id='seg' href='?page=$page'>Pàg. Següent</a>";
	} else {
		$page = $get_ + 1;
		if ($page < $pages){
			$pagAnt = $get_ - 1;
			echo "<a id='ant' href='?page=$pagAnt'>Pàg. Anterior</a>";
			echo "<a id='seg' href='?page=$page'>Pàg. Següent</a>";	
		} else {
			$pagAnt = $get_ - 1;
			echo "<a id='ant' href='?page=$pagAnt'>Pàg. Anterior</a>";
			echo "<a id='seg' style='pointer-events: none; color: grey;' href='?page=$page'>Pàg. Següent</a>";
		}

		$page = $get_ + 1;
	}
	
	$_SESSION['page'] = $page;
	
	echo '</div>';
}


function getModalDivToEditContact() {
	
	$div_modal = "";
	$div_modal .= '<div id="myModal" class="modal">';
	$div_modal .= '	<div class="modal-content">';
	$div_modal .= ' 		<div class="modal-header">';
	$div_modal .= '  			<span class="close" style="padding-top:0%;">×</span>';
	$div_modal .= '	  		<h1><i>Editar contacte</i></h1>';
	$div_modal .= '		</div>';
	
	$div_modal .= '		<div class="modal-body">';
	$div_modal .= getContactToEdit();
	$div_modal .= '		</div>';
	
	$div_modal .= ' 		<div class="modal-footer">';
	$div_modal .= '	  		<h5><i>Agenda Online</i></h5>';
	$div_modal .= '  		</div>';
	$div_modal .= '	</div>';
	$div_modal .= '</div>';
	
	return $div_modal;
}


function remarkSearchedValue($searchedValue, $value) { 	
	
	if ( substr_count($value, $searchedValue) > 0 ){
		
		$startSpan = '<span style="background-color:cyan;">'; 
		$markedResult = str_replace(
			$searchedValue, 
			$startSpan.$searchedValue.'</span>', 
			$value
		);
		
		return ucfirst($markedResult);
	} else
		return ucfirst($value);		
}

?>
