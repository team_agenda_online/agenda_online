<?php
	require 'inc/functions.php';
	session_start();
	
	// Si no ha iniciat sessió però intenta entrar en una atra pàgina se'l redirigeix a 'no_session.php'
	if ( !isset($_SESSION['username']) )
		header('location:no_session.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Insereix un usuari - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/inscribe.css">
</head>
<body>
<div id="container">
	<div id="header">
		<span id="topButtons" style="float: right;"><a href="logout.php"><img src="img/logout_20_white.png">Desconnectar-se</a></span>
		<span id="topButtons" style="float: left;"><a href="page.php"><img src="img/home_20_white.png">Inici</a></span>
	</div>

	<div id="content">

<?php
	if( isset($_POST['create']) ) {
		createUser();
	} else {
?>
	
	<h1 style="text-align: center;">Inscriu un contacte</h1>

	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<input type="text" placeholder="Nom" name="name" required><br><br>
		<input type="text" placeholder="Cognoms" name="surname"><br><br>
		<input type="text" placeholder="Correu electrònic" name="email" required><br><br>
		<input type="text" placeholder="Telèfon" name="phone" required><br><br>
		<input type="text" placeholder="Domicili" name="address" required><br><br>
		<input type="text" placeholder="aaaa/mm/dd" name="birthday" onclick="showIframe()" required><br><br>
		<input type="submit" name="create" value="Insereix">
	</form>
	
	<div class="divIframe" style="display: none; margin-left: -25%; margin-bottom: 40%;">
		<iframe
			name="iframecalendar" 
			scrolling="no" 
			frameborder="0" 
			width="100%" 
			height="100%" 
			src="js/calendar.html" 
			style="position: absolute;">
		</iframe>
	</div>
	
<?php
	}
?>
</div>
<script type="text/javascript" src="js/calendar.js"></script>
</body>
</html>