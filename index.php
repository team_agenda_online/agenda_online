<?php
	session_start();
	require 'inc/functions.php';
	
	if ( isset($_SESSION['username']) )
		header('location:page.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" >
	<title>Login - Agenda Online</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<body>
	<div id="login">
		<div id="logo">
			<h1>AGENDA ONLINE</h1>
		</div>
		<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<input type="text" name="username" placeholder="Usuari" required><br><br>
			<input type="password" name="password" placeholder="Contrasenya" required><br><br>
			<input type="submit" value="Login" name="login">
		</form>
	</div>
</body>
</html>
<?php	
	if ( isset($_POST['login']) ){
		if ( validateUser() == true ) {
			$_SESSION['username'] = $_POST['username'];
			header("location:page.php");
		} else {
?>
			<script>
				var errorMsg = "<span style='color:red'>Usuari o contrasenya errònis</span>";
				var logo = document.getElementById("logo");
				logo.innerHTML += errorMsg;
			</script>
<?php
		}
	}
?>